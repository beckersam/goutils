package embedFsWrapper

import (
	"io/fs"
)

// Fix for go:embed file systems including the full path of the embedded files
// Adds a given string to the front of all requests
type FSWrapper struct {
	wrapped fs.FS
	toAdd   string
}

func NewFSWrapper(wraps fs.FS, appends string) *FSWrapper {
	return &FSWrapper{
		wrapped: wraps,
		toAdd:   appends,
	}
}

func (fs *FSWrapper) Open(name string) (fs.File, error) {
	return fs.wrapped.Open(fs.toAdd + name)
}
