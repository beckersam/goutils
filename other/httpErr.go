package other

import (
	"fmt"
	"net/http"
)

// Return an error over an http connection.
// The error will have the given return code `code`
// and a json encoded body with the field "id" set to `errId`
// and a field "message" set to the `message`
func HttpErr(w http.ResponseWriter, errId int, message string, code int) {
	w.WriteHeader(code)
	w.Header().Add("Content-Type", "application/json")
	fmt.Fprintf(w, "{\"id\": %d, \"message\": \"%s\"}", errId, message)
}
