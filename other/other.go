package other

// OutputIntoChannel takes a singular return value and sends it into the target channel.
// This is a wrapper for functions where a value can't be collected from directly.
// Example: goroutines
func OutputIntoChannel[T any](out T, target chan T) {
	target <- out
}

func Must[T any](val T, err error) T {
	if err != nil {
		panic(err)
	}
	return val
}

func IntoPointer[T any](val T) *T {
	return &val
}
