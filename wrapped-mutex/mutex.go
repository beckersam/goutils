package wrappedmutex

import "sync"

type Mutex[T any] struct {
	wrapped T
	lock    sync.Mutex
}

func New[T any](wrapped T) Mutex[T] {
	return Mutex[T]{
		wrapped: wrapped,
	}
}

func (m *Mutex[T]) Lock() {
	m.lock.Lock()
}

func (m *Mutex[T]) TryLock() bool {
	return m.lock.TryLock()
}

func (m *Mutex[T]) Unlock() {
	m.lock.Unlock()
}

func (m *Mutex[T]) Get() *T {
	return &m.wrapped
}
